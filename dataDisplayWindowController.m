//
//  dataDisplayWindowController.m
//
//          2Remember- A simple reminder program

//  2Remember
//
//
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import "dataDisplayWindowController.h"
#import "appuntamento.h"
#import "globals.h"


@implementation dataDisplayWindowController
+ (dataDisplayWindowController *)newInstance {
    return [[self alloc] init];
}

-(id)init {
    snoozeDelta=600;
    appuntamentoLocale=nil;
   // [self initDizionari];
    return [super init];
}

-(void)showWithTodo:(appuntamento*)appunt {
    //chiude tutte le altre finestre
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"ARRdataDisplayOpen" object:appunt];
    //Memorizziamo appuntamento
    [appuntamentoLocale release];
    [appunt retain];
    appuntamentoLocale=appunt;
    //mostriamoci
    [self showPanel:self];
    //osserva tutte le altre finestre
    [nc addObserver:self
           selector:@selector(hasToClose)
               name:@"ARRdataDisplayOpen"
            object:appunt];

    [nc addObserver:self
           selector:@selector(updateUI)
               name:@"ARRdataChanged"
             object:appunt];
}

-(void)awakeFromNib
{
    [self setupFormatters];
}


-(void)setupFormatters {
    NSDateFormatter	*dForm;
    dForm=[[NSDateFormatter alloc] initWithDateFormat:ARRdateTimeOrdering allowNaturalLanguage:YES];
    [iDate setFormatter:dForm];
}

/*-(void)initDizionari {
    attributiDescrizione=[[NSMutableDictionary alloc] init];
    [attributiDescrizione setObject:[NSFont systemFontOfSize:12] forKey: NSFontAttributeName];

    //attributiData=[[NSMutableDictionary alloc] init];
    //[attributiData setObject:[NSFont fontWithName:@"Monaco" size:9] forKey: NSFontAttributeName];
    //[attributiData setObject:[NSColor darkGrayColor] forKey: NSForegroundColorAttributeName];

    attributiDati=[[NSMutableDictionary alloc] init];
    [attributiDati setObject:[NSFont systemFontOfSize:12] forKey: NSFontAttributeName];
    //[attributiData setObject:[NSColor darkGrayColor] forKey: NSForegroundColorAttributeName];

    //attributiTag=[[NSMutableDictionary alloc] init];
    //[attributiTag setObject:[NSFont boldSystemFontOfSize:12] forKey: NSFontAttributeName];
    //[attributiData setObject:[NSColor darkGrayColor] forKey: NSForegroundColorAttributeName];
}*/

-(void)hasToClose {
        [[self window] close];
        [self autorelease];
}

/*-(void)updateUI {
    NSMutableAttributedString *outFor;
    NSAttributedString *tag;
    NSString *cont;
    NSAttributedString *contFor;
    
    
    if ([appuntamentoLocale isDeleted]==NO) {
        outFor=[[NSMutableAttributedString alloc] initWithString:@""];
        int tipo=[appuntamentoLocale kind];
//mettiamo person
        if (tipo==0||tipo==1||tipo==2) {
            tag=[[NSAttributedString alloc] initWithString:@"Person: " attributes:attributiDescrizione];
            cont=[[NSString alloc] initWithFormat:@"%@/n",[appuntamentoLocale person]];
            contFor=[[NSAttributedString alloc] initWithString:cont attributes:attributiDati];
            [outFor appendAttributedString:tag];
            [outFor appendAttributedString:contFor];
            [tag autorelease];
            [cont autorelease];
            [contFor autorelease];
        }
            
    } else {
        [self hasToClose];
    }

}*/

//UPDATE UI... Go with verbose!!!
-(void)updateUI {
    if ([appuntamentoLocale isDeleted]==NO) {
        int tipo=[appuntamentoLocale kind];
        //facciamo un switch!
        switch (tipo) {
            case 0: //Phone
                [iLabel1 setStringValue:@"Person:"];
                [iLabel2 setStringValue:@"Phone:"];
                [iLabel3 setStringValue:@""];
                [iLabel4 setStringValue:@""];
                [iData1 setStringValue:[appuntamentoLocale person]];
                [iData2 setStringValue:[appuntamentoLocale phone]];
                [iData3 setStringValue:@""];
                [iData4 setStringValue:@""];
                [iDate setObjectValue:[appuntamentoLocale date]];
                [iTime setStringValue:[appuntamentoLocale sTime]];
                [iNotes replaceCharactersInRange:NSMakeRange(0,0) withString:[appuntamentoLocale note]];
                [titoloBox setTitle:@"Phone To:"];
                break;
            case 1: //Meet
                [iLabel1 setStringValue:@"Person:"];
                [iLabel2 setStringValue:@"Place:"];
                [iLabel3 setStringValue:@"Phone:"];
                [iLabel4 setStringValue:@""];
                [iData1 setStringValue:[appuntamentoLocale person]];
                [iData2 setStringValue:[appuntamentoLocale place]];
                [iData3 setStringValue:[appuntamentoLocale phone]];
                [iData4 setStringValue:@""];
                [iDate setObjectValue:[appuntamentoLocale date]];
                [iTime setStringValue:[appuntamentoLocale sTime]];
                [iNotes replaceCharactersInRange:NSMakeRange(0,0) withString:[appuntamentoLocale note]];
                [titoloBox setTitle:@"Meet:"];
                break;
            case 2: //email
                [iLabel1 setStringValue:@"Person:"];
                [iLabel2 setStringValue:@"Email:"];
                [iLabel3 setStringValue:@""];
                [iLabel4 setStringValue:@""];
                [iData1 setStringValue:[appuntamentoLocale person]];
                [iData2 setStringValue:[appuntamentoLocale email]];
                [iData3 setStringValue:@""];
                [iData4 setStringValue:@""];
                [iDate setObjectValue:[appuntamentoLocale date]];
                [iTime setStringValue:[appuntamentoLocale sTime]];
                [iNotes replaceCharactersInRange:NSMakeRange(0,0) withString:[appuntamentoLocale note]];
                [titoloBox setTitle:@"Email:"];
                break;
            case 3: //goto
                [iLabel1 setStringValue:@"Place:"];
                [iLabel2 setStringValue:@""];
                [iLabel3 setStringValue:@""];
                [iLabel4 setStringValue:@""];
                [iData1 setStringValue:[appuntamentoLocale place]];
                [iData2 setStringValue:@""];
                [iData3 setStringValue:@""];
                [iData4 setStringValue:@""];
                [iDate setObjectValue:[appuntamentoLocale date]];
                [iTime setStringValue:[appuntamentoLocale sTime]];
                [iNotes replaceCharactersInRange:NSMakeRange(0,0) withString:[appuntamentoLocale note]];
                [titoloBox setTitle:@"Go To:"];
                break;
            case 4: //web
                [iLabel1 setStringValue:@"Website:"];
                [iLabel2 setStringValue:@""];
                [iLabel3 setStringValue:@""];
                [iLabel4 setStringValue:@""];
                [iData1 setStringValue:[appuntamentoLocale place]];
                [iData2 setStringValue:@""];
                [iData3 setStringValue:@""];
                [iData4 setStringValue:@""];
                [iDate setObjectValue:[appuntamentoLocale date]];
                [iTime setStringValue:[appuntamentoLocale sTime]];
                [iNotes replaceCharactersInRange:NSMakeRange(0,0) withString:[appuntamentoLocale note]];
                [titoloBox setTitle:@"Website:"];
                break;
            case 5: //buy
                [iLabel1 setStringValue:@"What:"];
                [iLabel2 setStringValue:@"Place"];
                [iLabel3 setStringValue:@""];
                [iLabel4 setStringValue:@""];
                [iData1 setStringValue:[appuntamentoLocale what]];
                [iData2 setStringValue:[appuntamentoLocale place]];
                [iData3 setStringValue:@""];
                [iData4 setStringValue:@""];
                [iDate setObjectValue:[appuntamentoLocale date]];
                [iTime setStringValue:[appuntamentoLocale sTime]];
                [iNotes replaceCharactersInRange:NSMakeRange(0,0) withString:[appuntamentoLocale note]];
                [titoloBox setTitle:@"Buy:"];
                break;
            case 6: //generic
                [iLabel1 setStringValue:@"To do:"];
                [iLabel2 setStringValue:@""];
                [iLabel3 setStringValue:@""];
                [iLabel4 setStringValue:@""];
                [iData1 setStringValue:[appuntamentoLocale todo]];
                [iData2 setStringValue:@""];
                [iData3 setStringValue:@""];
                [iData4 setStringValue:@""];
                [iDate setObjectValue:[appuntamentoLocale date]];
                [iTime setStringValue:[appuntamentoLocale sTime]];
                [iNotes replaceCharactersInRange:NSMakeRange(0,0) withString:[appuntamentoLocale note]];
                [titoloBox setTitle:@"To Do:"];
                break;
            default:
                NSLog(@"dataDisplayWindowController:updateUI:default");
        }
    }
}
            


-(IBAction)showPanel:(id)sender {

    if (!okButton) {
        NSWindow *theWindow;

        if (![NSBundle loadNibNamed:@"dataDisplay" owner:self]) {
            NSLog(@"Failed to load dataDisplay.nib");
            return;
        }

        theWindow= [self window];
        //deve mettere il titolo

        [theWindow setLevel:NSModalPanelWindowLevel];
        [theWindow setExcludedFromWindowsMenu:NO];
        [theWindow setMenu:nil];
        [theWindow center];
    }
    [self sliderAction:self];
    [self updateUI];
    [[self window] makeKeyAndOrderFront:self];
}

- (IBAction)okButton:(id)sender {
//cancello appuntamento e mi chiudo
//I cancel the appointment and quit                                             //leg20230914 - Translation
    [appuntamentoLocale mightStopAlarm];
    [self hasToClose];
}

- (IBAction)sliderAction:(id)sender{
    int valore=[slider intValue];
    switch (valore)
    {
        case 0:
            snoozeDelta=5;
            [testoSlider setStringValue:@"5 Mins."];
            break;
        case 1:
            snoozeDelta=10;
            [testoSlider setStringValue:@"10 Mins."];
                break;
        case 2:
            snoozeDelta=20;
            [testoSlider setStringValue:@"20 Mins."];
            break;
        case 3:
            snoozeDelta=30;
            [testoSlider setStringValue:@"30 Mins."];
            break;
        case 4:
            snoozeDelta=45;
            [testoSlider setStringValue:@"45 Mins."];
            break;
        case 5:
            snoozeDelta=60;
            [testoSlider setStringValue:@"1 Hour"];
            break;
        case 6:
            snoozeDelta=120;
            [testoSlider setStringValue:@"2 Hour"];
            break;
        case 7:
            snoozeDelta=240;
            [testoSlider setStringValue:@"4 Hour"];
            break;
        case 8:
            snoozeDelta=480;
            [testoSlider setStringValue:@"8 Hour"];
            break;
        default:
            snoozeDelta=10;
    }
            
}

- (IBAction)snoozeButton:(id)sender{
    [appuntamentoLocale snooze:snoozeDelta];
    [self hasToClose];
}

-(void)dealloc {
    [[self window] autorelease];
    [appuntamentoLocale release];
    [super dealloc];
}
@end
