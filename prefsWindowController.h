//
//  prefsWindowController.h
//
//          2Remember- A simple reminder program

//  2Remember
//
//
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import <AppKit/AppKit.h>


@interface prefsWindowController : NSWindowController {
    IBOutlet id blinkButton;
    IBOutlet id bounceButton;
    IBOutlet id beepButton;
    IBOutlet id alertButton;
    IBOutlet id startUpButton;
    IBOutlet id sortingMenu;
    IBOutlet id animationButton;
    IBOutlet id locAutoShow;
}
+ (prefsWindowController *)sharedInstance;

- (IBAction)setBlink:(id)sender;
- (IBAction)setBounce:(id)sender;
- (IBAction)setBeep:(id)sender;
- (IBAction)setAlert:(id)sender;
- (IBAction)setStartup:(id)sender;
- (IBAction)setSorting:(id)sender;
- (IBAction)showPanel:(id)sender;
- (void) updateUI;
- (IBAction)setAnimation:(id)sender;
- (IBAction)setAutoShow:(id)sender;
@end
