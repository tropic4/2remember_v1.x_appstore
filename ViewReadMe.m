//
//  ViewReadMe.m
//  iAddressX
//
//  Created by Lewis Garrett on 2/10/15.
//
//

#import "ViewReadMe.h"

@interface ViewReadMe ()

@end

@implementation ViewReadMe

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (void)awakeFromNib
{
    NSBundle * myMainBundle = [NSBundle mainBundle];

    // Fix Issue #148 - MAS version needs a separate ReadMe file.               //leg20201020 - MAS 4.0.0
    //  Modified so "ReadMe_Apple.rtf" is used for MAS version.
    NSString * rtfFilePath;
//#if IAD_STORETARGET_TROPICAL
    rtfFilePath = [myMainBundle pathForResource:@"ReadMe" ofType:@"rtf"];
//#else
//    rtfFilePath = [myMainBundle pathForResource:@"ReadMe_Apple" ofType:@"rtf"];
//#endif
    
    [_readMeView readRTFDFromFile:rtfFilePath];
    [self.window setTitleWithRepresentedFilename:rtfFilePath];
    
    // Why doesn't setShowsResizeIndicator work?  I thought it would            //leg20150210 - 3.6.0
    //  prevent resizing.  Setting min/max to initial size effectively
    //  disables resizing.
    [self.window setShowsResizeIndicator:NO];
    [self.window setMaxSize:self.window.frame.size];
    [self.window setMinSize:self.window.frame.size];
    
    // Fix Issue #125 - "View ReadMe almost unreadable in Dark Mode"            //leg20200806 - 4.0.0
    //  Insure "View ReadMe" Appearance is Aqua regardless of Dark Mode.
    if (@available(macOS 10.9, *)) {
        _readMeView.appearance = [NSAppearance appearanceNamed: NSAppearanceNameAqua];
    } else {
        // Not an Issue prior to Dark Mode option in MacOS.
    }
    
    [_readMeView setEditable:NO];
}

@end

