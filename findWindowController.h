//
//  findWindowController.h
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Sun Mar 23 2003.
//  Version 1.0-1.0.5 by Andrea Rincon Ray on or beforer Sun Feb 09 2003.
//  Versions after 1.0.6 by James Lee and Lewis Garrett, Tropical Software, Inc. 

//  2Remember
//  Copyright (c) 2011 Tropical Software, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <AppKit/AppKit.h>


@interface findWindowController : NSWindowController {
    IBOutlet NSTableView *tabella;
    NSArray *tableData;
}
- (IBAction)selected:(id)sender;
-(void)setTableData:(NSArray*)newData;
-(void)sveglia;
-(NSTableView*)tabella;
@end
