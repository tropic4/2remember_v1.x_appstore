//
//  AppController.h
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Fri Feb 07 2003.
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

@class appuntamento;
@class ViewReadMe;                                                              //leg20201204 - 1.3.0


@interface AppController : NSObject {
    int		numeroAllarmi;
    NSTimer	*timerGenerale;
    NSTimer	*timerSpegni;
    BOOL	registerMenu;

    ViewReadMe*                 _viewReadMeWindowController;                    //leg20201204 - 1.3.0
}
- (void)notifyOfAlarm:(appuntamento*)who;
- (void)endNotificationOfAlarm:(appuntamento*)who;
- (void)blinkON;
- (void)blinkOFF;
- (IBAction)showAboutBox:(id)sender;
- (IBAction)checkForNewVersion:(id)sender;
- (IBAction)showPreferences:(id)sender;

- (IBAction)newMenu:(id)sender;
- (IBAction)openMenu:(id)sender;

// Remove registration nag by commenting-out timer scheduling.                  //leg20180404 - 1.2.0
//- (IBAction)registrazione:(id)sender;

- (void)setRegisterMenu:(BOOL)val;
- (void)mainInterfaceDispatch:(int)event;
- (void)inizializzaPreferenze;
- (void)leggePreferenzeUtente;
- (void)salvaPreferenzeUtente;
- (void) salvataggioPeriodico;

@end
