/* tabView */

#import <Cocoa/Cocoa.h>

@interface tabView : NSView
{
    NSImage 		*myImage;
    NSImage 		*hiImage;
    BOOL 		hilight;
    BOOL 		visibile;
    BOOL		lockRects;
    NSTrackingRectTag	trackingRectTag;
}
-(void)setImage:(NSImage*)image also:(NSImage*)image2;
-(void)setHilight:(BOOL)risultato;
-(void)setVisible:(BOOL)risultato;
-(void)blinkOFF;
-(void)blinkON;
@end
