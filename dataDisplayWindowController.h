//
//  dataDisplayWindowController.h
//
//          2Remember- A simple reminder program

//  2Remember
//
//
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class appuntamento;

@interface dataDisplayWindowController : NSWindowController
{
    IBOutlet id iData1;
    IBOutlet id iData2;
    IBOutlet id iData3;
    IBOutlet id iData4;
    IBOutlet id iDate;
    IBOutlet id iLabel1;
    IBOutlet id iLabel2;
    IBOutlet id iLabel3;
    IBOutlet id iLabel4;
    IBOutlet id iNotes;
    IBOutlet id iTime;
    IBOutlet id okButton;
    IBOutlet id slider;
    IBOutlet id snoozeButton;
    IBOutlet id testoSlider;
    IBOutlet id titoloBox;

    appuntamento 		*appuntamentoLocale;
    int				snoozeDelta;
    NSMutableDictionary		*attributiDescrizione;
    NSMutableDictionary 	*attributiDati;
}

+ (dataDisplayWindowController *)newInstance;
- (IBAction)okButton:(id)sender;
- (IBAction)sliderAction:(id)sender;
- (IBAction)snoozeButton:(id)sender;
- (void)showWithTodo:(appuntamento*)appunt;
- (IBAction)showPanel:(id)sender;
- (void)hasToClose;
- (void)updateUI;
-(void)setupFormatters;
@end
